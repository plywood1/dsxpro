#!/usr/bin/env bash

git clone https://gitlab.com/srk97/travisx.git && cd travisx || return

npm i

./node_modules/.bin/ts-node build.ts
